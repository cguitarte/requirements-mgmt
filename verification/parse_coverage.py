# import csv module
import csv
import os
import glob
import json

# trace matrix name
filename = "artifacts/trace-matrix.csv"

test_status = {}

def parse_results():
    global test_status
    path = os.getcwd()

    for filename in glob.glob(os.path.join(path, '*.results')):
        with open(os.path.join(path, filename), 'r') as f:
            stat = f.readline().strip()
            test_status[os.path.basename(filename).replace(".results", "")] = stat
        f.close()


def generate_json():
    # initialize fields / rows lists
    fields = []
    rows = []
    json_data = {}

    # read trace matrix
    with open(filename, 'r') as csvfile:
        # create the CSV reader opject
        csvreader = csv.reader(csvfile)

        # extract field names (first row)
        fields = next(csvreader)

        # extract data fields row by row
        for row in csvreader:
            rows.append(row)

        # print out status
        print("Total number of requirements parsed: %d"%(csvreader.line_num - 1))
    
    # Close the file
    csvfile.close()

    # print out field names for good measure
    print('Field names are: ' + ', '.join(field for field in fields))

    # print out rows
    print('\n  Trace data:\n')
    for row in rows:
        for col in row:
            print("%10s"%col),
        print("%s\n"%(test_status[row[1]]))

    for row in rows:
        print('%s\n'%row[0])
        json_data[row[0]] = test_status[row[1]]

    with open('artifacts.json', 'w') as outfile:
        json.dump(json_data, outfile)

def parse_requirements():
    parse_results()
    generate_json()


if __name__ == "__main__":
    parse_requirements()